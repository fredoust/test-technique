<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    public function listAction()
    {      
        $sort_param = $this->params()->fromRoute('sort_param');
        $sort_direction = $this->params()->fromRoute('sort_direction');
        
        if($sort_param != '' && $sort_direction != '')
        {          
        $users = $this->getServiceLocator()->get('entity_manager')
            ->getRepository('Application\Entity\User')
            ->findBy(array(), array ($sort_param => $sort_direction));
        }else{
            
            $users = $this->getServiceLocator()->get('entity_manager')
            ->getRepository('Application\Entity\User')
            ->findAll();
             
            /*
              $users = $this->getServiceLocator()->get('entity_manager')
            ->getRepository('Application\Entity\User')
            ->findBy(array(), array ('profile.lastname' => 'DESC'));
              */
          
        }

        return new ViewModel(array(
            'users' =>  $users
        ));
    }
    
   

    public function addAction()
    {
        
        /* @var $form \Application\Form\UserForm */
        $form = $this->getServiceLocator()->get('formElementManager')->get('form.user');

        $data = $this->prg();

        if ($data instanceof \Zend\Http\PhpEnvironment\Response) {
            return $data;
        }

        if ($data != false) {
            $form->setData($data);
            if ($form->isValid()) {
 
                /* @var $user \Application\Entity\User */
                $user = $form->getData();

                /* @var $serviceUser \Application\Service\UserService */
                $serviceUser = $this->getServiceLocator()->get('application.service.user');
                $serviceUser->saveUser($user);
                $this->redirect()->toRoute('users');
               
            }else{
               
            }
        }

        return new ViewModel(array(
            'form'  =>  $form
        ));
    }

    public function removeAction()
    {
        //To do : Do Remove User
        
        $form = $this->getServiceLocator()->get('formElementManager')->get('form.user');
        
        $userToRemove = $this->getServiceLocator()->get('entity_manager')
            ->getRepository('Application\Entity\User')
            ->find($this->params()->fromRoute('user_id'));
        
        /* @var $serviceUser \Application\Service\UserService */
        $serviceUser = $this->getServiceLocator()->get('application.service.user');
        
        $serviceUser->removeUser($userToRemove);

        $this->redirect()->toRoute('users');
        
        
        return new ViewModel(array(
            'form'  =>  $form
        ));
        
         
    }

    public function editAction()
    {
        /* @var $form \Application\Form\UserForm */
        $form = $this->getServiceLocator()->get('formElementManager')->get('form.user');
        $userToEdit = $this->getServiceLocator()->get('entity_manager')
            ->getRepository('Application\Entity\User')
            ->find($this->params()->fromRoute('user_id'));

        $form->bind($userToEdit);
        $form->get('firstname')->setValue($userToEdit->getFirstName());
        $form->get('lastname')->setValue($userToEdit->getLastName());
        $form->get('address')->setValue($userToEdit->getAddress());
        $form->get('birthday')->setValue($userToEdit->getBirthday());

        $data = $this->prg();

        if ($data instanceof \Zend\Http\PhpEnvironment\Response) {
            return $data;
        }

        if ($data != false) {
            $form->setData($data);
            if ($form->isValid()) {

                /* @var $user \Application\Entity\User */
                $user = $form->getData();

                /* @var $serviceUser \Application\Service\UserService */
                $serviceUser = $this->getServiceLocator()->get('application.service.user');

                $serviceUser->saveUser($user);

                $this->redirect()->toRoute('users');
            }
        }

        return new ViewModel(array(
            'form'  =>  $form
        ));
    }

}